package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.CityInfo;
import com.ruoyi.system.service.ICityInfoService;
import com.ruoyi.system.service.IProvinceInfoService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 城市信息Controller
 *
 * @author zebra
 * @date 2020-06-05
 */
@Controller
@RequestMapping("/bussiness/cityinfo")
public class CityInfoController extends BaseController {
    private String prefix = "system/cityinfo";

    @Autowired
    private ICityInfoService cityInfoService;
    @Autowired
    private IProvinceInfoService provinceInfoService;

    @RequiresPermissions("bussiness:cityinfo:view")
    @GetMapping()
    public String cityinfo(ModelMap mmap) {
        mmap.addAttribute("province", provinceInfoService.selectProvinceInfoList(null));
        return prefix + "/cityinfo";
    }

    /**
     * 查询城市信息列表
     */
    @RequiresPermissions("bussiness:cityinfo:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CityInfo cityInfo) {
        startPage();
        List<CityInfo> list = cityInfoService.selectCityInfoList(cityInfo);
        return getDataTable(list);
    }

    /**
     * 新增城市信息
     */
    @GetMapping("/add")
    public String add(ModelMap mmap) {
        mmap.addAttribute("province", provinceInfoService.selectProvinceInfoList(null));
        return prefix + "/add";
    }

    /**
     * 新增保存城市信息
     */
    @RequiresPermissions("bussiness:cityinfo:add")
    @Log(title = "城市信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CityInfo cityInfo) {
        if (cityInfoService.selectCityInfoById(cityInfo.getCityCode()) != null) {
            return error("该城市编码已经存在！");
        }
        return toAjax(cityInfoService.insertCityInfo(cityInfo));
    }

    /**
     * 修改城市信息
     */
    @GetMapping("/edit/{cityCode}")
    public String edit(@PathVariable("cityCode") String cityCode, ModelMap mmap) {
        mmap.addAttribute("province", provinceInfoService.selectProvinceInfoList(null));
        CityInfo cityInfo = cityInfoService.selectCityInfoById(cityCode);
        mmap.put("cityInfo", cityInfo);
        return prefix + "/edit";
    }

    /**
     * 修改保存城市信息
     */
    @RequiresPermissions("bussiness:cityinfo:edit")
    @Log(title = "城市信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CityInfo cityInfo) {
        return toAjax(cityInfoService.updateCityInfo(cityInfo));
    }

    /**
     * 删除城市信息
     */
    @RequiresPermissions("bussiness:cityinfo:remove")
    @Log(title = "城市信息", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(cityInfoService.deleteCityInfoByIds(ids));
    }
}
