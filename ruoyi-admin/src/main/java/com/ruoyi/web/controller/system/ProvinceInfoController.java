package com.ruoyi.web.controller.system;


import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ProvinceInfo;
import com.ruoyi.system.service.IProvinceInfoService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 省份信息Controller
 *
 * @author zebra
 * @date 2020-06-05
 */
@Controller
@RequestMapping("/bussiness/provinceinfo")
public class ProvinceInfoController extends BaseController {
    private String prefix = "system/provinceinfo";

    @Autowired
    private IProvinceInfoService provinceInfoService;

    @RequiresPermissions("bussiness:provinceinfo:view")
    @GetMapping()
    public String provinceinfo() {
        return prefix + "/provinceinfo";
    }

    /**
     * 查询省份信息列表
     */
    @RequiresPermissions("bussiness:provinceinfo:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ProvinceInfo provinceInfo) {
        startPage();
        List<ProvinceInfo> list = provinceInfoService.selectProvinceInfoList(provinceInfo);
        return getDataTable(list);
    }

    /**
     * 新增省份信息
     */
    @GetMapping("/add")
    public String add(ModelMap mmap) {
        mmap.addAttribute("province", provinceInfoService.selectProvinceInfoList(null));
        return prefix + "/add";
    }

    /**
     * 新增保存省份信息
     */
    @RequiresPermissions("bussiness:provinceinfo:add")
    @Log(title = "省份信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ProvinceInfo provinceInfo) {
        if (provinceInfoService.selectProvinceInfoById(provinceInfo.getProvinceId()) != null) {
            return error("该省份编码已经存在！");
        }
        return toAjax(provinceInfoService.insertProvinceInfo(provinceInfo));
    }

    /**
     * 修改省份信息
     */
    @GetMapping("/edit/{cityCode}")
    public String edit(@PathVariable("cityCode") String proviceId, ModelMap mmap) {
        mmap.addAttribute("province", provinceInfoService.selectProvinceInfoList(null));
        ProvinceInfo provinceInfo = provinceInfoService.selectProvinceInfoById(proviceId);
        mmap.put("provinceInfo", provinceInfo);
        return prefix + "/edit";
    }

    /**
     * 修改保存省份信息
     */
    @RequiresPermissions("bussiness:provinceinfo:edit")
    @Log(title = "省份信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ProvinceInfo proviceInfo) {
        return toAjax(provinceInfoService.updateProvinceInfo(proviceInfo));
    }

    /**
     * 删除省份信息
     */
    @RequiresPermissions("bussiness:provinceinfo:remove")
    @Log(title = "省份信息", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(provinceInfoService.deleteProvinceInfoByIds(ids));
    }
}
