package com.ruoyi.smdd.ddgl.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.smdd.ddgl.domain.KbsmOrder;
import com.ruoyi.smdd.ddgl.service.IKbsmOrderService;
import com.ruoyi.smdd.ddgl.service.IKbsmOrderService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 订单管理Controller
 *
 * @author ruoyi
 * @date 2020-08-04
 */
@Controller
@RequestMapping("/smdd/ddgl")
public class KbsmOrderController extends BaseController {
    private String prefix = "smdd/ddgl";

    @Autowired
    private IKbsmOrderService kbsmOrderService;

    @RequiresPermissions("smdd:ddgl:view")
    @GetMapping()
    public String ddgl() {
        return prefix + "/ddgl";
    }

    /**
     * 查询订单管理列表
     */
    @RequiresPermissions("smdd:ddgl:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(KbsmOrder kbsmOrder) {
        startPage();
        List<KbsmOrder> list = kbsmOrderService.selectKbsmOrderList(kbsmOrder);
        return getDataTable(list);
    }

    /**
     * 导出订单管理列表
     */
    @RequiresPermissions("smdd:ddgl:export")
    @Log(title = "订单管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(KbsmOrder kbsmOrder) {
        List<KbsmOrder> list = kbsmOrderService.selectKbsmOrderList(kbsmOrder);
        ExcelUtil<KbsmOrder> util = new ExcelUtil<KbsmOrder>(KbsmOrder.class);
        return util.exportExcel(list, "ddgl");
    }

    /**
     * 新增订单管理
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存订单管理
     */
    @RequiresPermissions("smdd:ddgl:add")
    @Log(title = "订单管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(KbsmOrder kbsmOrder) {
        return toAjax(kbsmOrderService.insertKbsmOrder(kbsmOrder));
    }

    /**
     * 修改订单管理
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        KbsmOrder kbsmOrder = kbsmOrderService.selectKbsmOrderById(id);
        mmap.put("kbsmOrder", kbsmOrder);
        return prefix + "/edit";
    }

    /**
     * 修改保存订单管理
     */
    @RequiresPermissions("smdd:ddgl:edit")
    @Log(title = "订单管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(KbsmOrder kbsmOrder) {
        return toAjax(kbsmOrderService.updateKbsmOrder(kbsmOrder));
    }

    /**
     * 删除订单管理
     */
    @RequiresPermissions("smdd:ddgl:remove")
    @Log(title = "订单管理", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(kbsmOrderService.deleteKbsmOrderByIds(ids));
    }
}
