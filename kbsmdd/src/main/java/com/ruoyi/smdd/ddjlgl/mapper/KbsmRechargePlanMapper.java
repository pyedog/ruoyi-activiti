package com.ruoyi.smdd.ddjlgl.mapper;

import com.ruoyi.smdd.ddjlgl.domain.KbsmRechargePlan;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hnkb
 * @since 2020-07-21
 */
public interface KbsmRechargePlanMapper{

    KbsmRechargePlan getByRechargePlanId(Integer rechargePlanId);

    KbsmRechargePlan getByWxAppId(Integer wxappId);

    List<KbsmRechargePlan> selectByWxAppId(Integer wxappId);
}
