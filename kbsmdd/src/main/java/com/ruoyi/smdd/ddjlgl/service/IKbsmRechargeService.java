package com.ruoyi.smdd.ddjlgl.service;

import com.ruoyi.smdd.ddjlgl.domain.KbsmRecharge;
import com.ruoyi.wxapi.model.qo.BaseParamQO;
import com.ruoyi.wxapi.model.qo.RechargeQO;

import java.util.List;

/**
 * 订单记录管理Service接口
 * 
 * @author ruoyi
 * @date 2020-08-04
 */
public interface IKbsmRechargeService 
{
    public KbsmRecharge addRecharge(BaseParamQO baseParamQO, RechargeQO rechargeQO) ;

    public KbsmRecharge getByOrderNo(long orderNo);

    /**
     * 查询订单记录管理
     * 
     * @param id 订单记录管理ID
     * @return 订单记录管理
     */
    public KbsmRecharge selectKbsmRechargeById(Long id);

    /**
     * 查询订单记录管理列表
     * 
     * @param kbsmRecharge 订单记录管理
     * @return 订单记录管理集合
     */
    public List<KbsmRecharge> selectKbsmRechargeList(KbsmRecharge kbsmRecharge);

    /**
     * 新增订单记录管理
     * 
     * @param kbsmRecharge 订单记录管理
     * @return 结果
     */
    public int insertKbsmRecharge(KbsmRecharge kbsmRecharge);

    /**
     * 修改订单记录管理
     * 
     * @param kbsmRecharge 订单记录管理
     * @return 结果
     */
    public int updateKbsmRecharge(KbsmRecharge kbsmRecharge);

    /**
     * 批量删除订单记录管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteKbsmRechargeByIds(String ids);

    /**
     * 删除订单记录管理信息
     * 
     * @param id 订单记录管理ID
     * @return 结果
     */
    public int deleteKbsmRechargeById(Long id);
}
