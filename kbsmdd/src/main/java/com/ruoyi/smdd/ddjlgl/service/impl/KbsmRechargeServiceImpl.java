package com.ruoyi.smdd.ddjlgl.service.impl;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.smdd.ddjlgl.domain.KbsmRecharge;
import com.ruoyi.smdd.ddjlgl.mapper.KbsmRechargeMapper;
import com.ruoyi.smdd.ddjlgl.service.IKbsmRechargeService;
import com.ruoyi.wxapi.controller.util.JWTUtil;
import com.ruoyi.wxapi.model.domain.Recharge;
import com.ruoyi.wxapi.model.domain.RechargePlan;
import com.ruoyi.wxapi.model.mapper.RechargePlanMapper;
import com.ruoyi.wxapi.model.qo.BaseParamQO;
import com.ruoyi.wxapi.model.qo.RechargeQO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 订单记录管理Service业务层处理
 *
 * @author ruoyi
 * @date 2020-08-04
 */
@Service
public class KbsmRechargeServiceImpl implements IKbsmRechargeService {
    @Autowired
    private KbsmRechargeMapper kbsmRechargeMapper;
    @Autowired
    RechargePlanMapper rechargePlanMapper;

    @Override
    public KbsmRecharge addRecharge(BaseParamQO baseParamQO, RechargeQO rechargeQO) {
        Snowflake snowflake = IdUtil.getSnowflake(1, 1);
        long orderNo = snowflake.nextId();
        KbsmRecharge recharge = null;
        if (rechargeQO.getRecharge_plan_id() > 0) {
            // 套餐充值
            RechargePlan rechargePlan = rechargePlanMapper.getByRechargePlanId(rechargeQO.getRecharge_plan_id());
            recharge = new KbsmRecharge();
            recharge.setOrderNo(orderNo);
            recharge.setMoney(Long.valueOf(rechargePlan.getMoney()));
            recharge.setGiftMoney(Long.valueOf(rechargePlan.getGiftMoney()));
            recharge.setUserId(Long.valueOf(JWTUtil.parseJWT(baseParamQO.getToken())));
            recharge.setShopId(Long.valueOf(baseParamQO.getShop_id()));
            recharge.setWxappId(Long.valueOf(baseParamQO.getWxapp_id()));
            recharge.setRechargePlanId(Long.valueOf(rechargePlan.getId()));

        } else {
            // 直接充值
            recharge = new KbsmRecharge();
            recharge.setOrderNo(orderNo);
            recharge.setMoney(Long.valueOf(rechargeQO.getMoney()));
            recharge.setUserId(Long.valueOf(JWTUtil.parseJWT(baseParamQO.getToken())));
            recharge.setShopId(Long.valueOf(baseParamQO.getShop_id()));
            recharge.setWxappId(Long.valueOf(baseParamQO.getWxapp_id()));

        }
        int flag = this.insertKbsmRecharge(recharge);
        if (flag > 0) {
            return recharge;
        }
        return null;
    }

    /**
     * 查询订单记录管理
     *
     * @param id 订单记录管理ID
     * @return 订单记录管理
     */
    @Override
    public KbsmRecharge selectKbsmRechargeById(Long id) {
        return kbsmRechargeMapper.selectKbsmRechargeById(id);
    }

    /**
     * 查询订单记录管理列表
     *
     * @param kbsmRecharge 订单记录管理
     * @return 订单记录管理
     */
    @Override
    public List<KbsmRecharge> selectKbsmRechargeList(KbsmRecharge kbsmRecharge) {
        return kbsmRechargeMapper.selectKbsmRechargeList(kbsmRecharge);
    }

    /**
     * 新增订单记录管理
     *
     * @param kbsmRecharge 订单记录管理
     * @return 结果
     */
    @Override
    public int insertKbsmRecharge(KbsmRecharge kbsmRecharge) {
        return kbsmRechargeMapper.insertKbsmRecharge(kbsmRecharge);
    }

    /**
     * 修改订单记录管理
     *
     * @param kbsmRecharge 订单记录管理
     * @return 结果
     */
    @Override
    public int updateKbsmRecharge(KbsmRecharge kbsmRecharge) {
        return kbsmRechargeMapper.updateKbsmRecharge(kbsmRecharge);
    }

    /**
     * 删除订单记录管理对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteKbsmRechargeByIds(String ids) {
        return kbsmRechargeMapper.deleteKbsmRechargeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除订单记录管理信息
     *
     * @param id 订单记录管理ID
     * @return 结果
     */
    @Override
    public int deleteKbsmRechargeById(Long id) {
        return kbsmRechargeMapper.deleteKbsmRechargeById(id);
    }

    @Override
    public KbsmRecharge getByOrderNo(long orderNo) {
        return this.getByOrderNo(orderNo);
    }
}
