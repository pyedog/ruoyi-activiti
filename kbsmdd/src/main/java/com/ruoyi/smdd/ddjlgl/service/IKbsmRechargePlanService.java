package com.ruoyi.smdd.ddjlgl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.smdd.ddjlgl.domain.KbsmRechargePlan;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hnkb
 * @since 2020-07-21
 */
public interface IKbsmRechargePlanService extends IService<KbsmRechargePlan> {

    KbsmRechargePlan getByWxAppId(@Param("wxappId") Integer wxappId);

    List<KbsmRechargePlan> selectByWxAppId(@Param("wxappId") Integer wxappId);
}
