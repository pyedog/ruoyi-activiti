package com.ruoyi.smdd.ggzgl.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.smdd.ggzgl.domain.KbsmGoodsSpecGroup;
import com.ruoyi.smdd.ggzgl.service.IKbsmGoodsSpecGroupService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 规格组管理Controller
 *
 * @author ruoyi
 * @date 2020-08-10
 */
@Controller
@RequestMapping("/smdd/ggzgl")
public class KbsmGoodsSpecGroupController extends BaseController {
    private String prefix = "smdd/ggzgl";

    @Autowired
    private IKbsmGoodsSpecGroupService kbsmGoodsSpecGroupService;

    @RequiresPermissions("smdd:ggzgl:view")
    @GetMapping()
    public String ggzgl() {
        return prefix + "/ggzgl";
    }

    /**
     * 查询规格组管理列表
     */
    @RequiresPermissions("smdd:ggzgl:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(KbsmGoodsSpecGroup kbsmGoodsSpecGroup) {
        startPage();
        List<KbsmGoodsSpecGroup> list = kbsmGoodsSpecGroupService.selectKbsmGoodsSpecGroupList(kbsmGoodsSpecGroup);
        return getDataTable(list);
    }

    /**
     * 导出规格组管理列表
     */
    @RequiresPermissions("smdd:ggzgl:export")
    @Log(title = "规格组管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(KbsmGoodsSpecGroup kbsmGoodsSpecGroup) {
        List<KbsmGoodsSpecGroup> list = kbsmGoodsSpecGroupService.selectKbsmGoodsSpecGroupList(kbsmGoodsSpecGroup);
        ExcelUtil<KbsmGoodsSpecGroup> util = new ExcelUtil<KbsmGoodsSpecGroup>(KbsmGoodsSpecGroup.class);
        return util.exportExcel(list, "ggzgl");
    }

    /**
     * 新增规格组管理
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存规格组管理
     */
    @RequiresPermissions("smdd:ggzgl:add")
    @Log(title = "规格组管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(KbsmGoodsSpecGroup kbsmGoodsSpecGroup) {
        return toAjax(kbsmGoodsSpecGroupService.insertKbsmGoodsSpecGroup(kbsmGoodsSpecGroup));
    }

    /**
     * 修改规格组管理
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Integer id, ModelMap mmap) {
        KbsmGoodsSpecGroup kbsmGoodsSpecGroup = kbsmGoodsSpecGroupService.selectKbsmGoodsSpecGroupById(id);
        mmap.put("kbsmGoodsSpecGroup", kbsmGoodsSpecGroup);
        return prefix + "/edit";
    }

    /**
     * 修改保存规格组管理
     */
    @RequiresPermissions("smdd:ggzgl:edit")
    @Log(title = "规格组管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(KbsmGoodsSpecGroup kbsmGoodsSpecGroup) {
        return toAjax(kbsmGoodsSpecGroupService.updateKbsmGoodsSpecGroup(kbsmGoodsSpecGroup));
    }

    /**
     * 删除规格组管理
     */
    @RequiresPermissions("smdd:ggzgl:remove")
    @Log(title = "规格组管理", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(kbsmGoodsSpecGroupService.deleteKbsmGoodsSpecGroupByIds(ids));
    }
}
