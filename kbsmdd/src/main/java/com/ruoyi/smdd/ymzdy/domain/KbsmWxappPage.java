package com.ruoyi.smdd.ymzdy.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 页面自定义对象 kbsm_wxapp_page
 *
 * @author ruoyi
 * @date 2020-08-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class KbsmWxappPage implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 页面id(主键)
     */
    private Long id;

    /**
     * 页面类型(1首页 2自定义页)
     */
    @Excel(name = "页面类型(1首页 2自定义页)")
    private Integer pageType;

    /**
     * 页面数据
     */
    @Excel(name = "页面数据")
    private String pageData;

    /**
     * 微信小程序id
     */
    @Excel(name = "微信小程序id")
    private Long wxappId;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtCreate;

    /**
     * 更新时间
     */
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtModified;

}
