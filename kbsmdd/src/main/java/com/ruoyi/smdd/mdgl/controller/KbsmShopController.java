package com.ruoyi.smdd.mdgl.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.smdd.mdgl.domain.KbsmShop;
import com.ruoyi.smdd.mdgl.service.IKbsmShopService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * 门店管理Controller
 *
 * @author ruoyi
 * @date 2020-08-04
 */
@Controller
@RequestMapping("/smdd/mdgl")
public class KbsmShopController extends BaseController {
    private String prefix = "smdd/mdgl";

    @Autowired
    private IKbsmShopService kbsmShopService;

    @RequiresPermissions("smdd:mdgl:view")
    @GetMapping()
    public String mdgl() {
        return prefix + "/mdgl";
    }

    /**
     * 查询门店管理列表
     */
    @RequiresPermissions("smdd:mdgl:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(KbsmShop kbsmShop) {
        startPage();
        List<KbsmShop> list = kbsmShopService.selectKbsmShopList(kbsmShop);
        return getDataTable(list);
    }

    /**
     * 导出门店管理列表
     */
    @RequiresPermissions("smdd:mdgl:export")
    @Log(title = "门店管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(KbsmShop kbsmShop) {
        List<KbsmShop> list = kbsmShopService.selectKbsmShopList(kbsmShop);
        ExcelUtil<KbsmShop> util = new ExcelUtil<KbsmShop>(KbsmShop.class);
        return util.exportExcel(list, "mdgl");
    }

    /**
     * 新增门店管理
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存门店管理
     */
    @RequiresPermissions("smdd:mdgl:add")
    @Log(title = "门店管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(KbsmShop kbsmShop) {
        return toAjax(kbsmShopService.insertKbsmShop(kbsmShop));
    }

    /**
     * 修改门店管理
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        KbsmShop kbsmShop = kbsmShopService.selectKbsmShopById(id);
        mmap.put("kbsmShop", kbsmShop);
        return prefix + "/edit";
    }

    /**
     * 修改保存门店管理
     */
//    @RequiresPermissions("smdd:mdgl:edit")
//    @Log(title = "门店管理", businessType = BusinessType.UPDATE)
//    @PostMapping("/edit")
//    @ResponseBody
//    public AjaxResult editSave(KbsmShop kbsmShop) {
//        return toAjax(kbsmShopService.updateKbsmShop(kbsmShop));
//    }

    /**
     * 删除门店管理
     */
    @RequiresPermissions("smdd:mdgl:remove")
    @Log(title = "门店管理", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(kbsmShopService.deleteKbsmShopByIds(ids));
    }

    /**
     * 修改保存产品信息
     */
    @RequiresPermissions("smdd:mdgl:edit")
    @Log(title = "产品信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(KbsmShop kbsmShop, @RequestParam("pic_file") MultipartFile pic_file) throws IOException {
        KbsmShop info = kbsmShopService.selectKbsmShopById(kbsmShop.getId());
        if (info == null) {
            return error("产品信息不存在");
        }
//        super.getMerchantById(info.getMerchantId());
//        commodityInfo.setMerchantId(info.getMerchantId());
//        String fileUrl = saveFile(pic_file, "", prefix, false);
        String fileUrl = FileUploadUtils.upload(pic_file);
        kbsmShop.setLogoImageId(fileUrl);
//        commodityInfo.setExamineStatus(info.getExamineStatus());
//        commodityInfo.setExamineDesc(null);
//        commodityInfo.setMerchantId(info.getMerchantId());
        return toAjax(kbsmShopService.updateKbsmShop(kbsmShop));
    }

//    protected String saveFile(MultipartFile pic_file, String fileName, String path, Boolean isMast) {
//        try {
//            fileName += ".png";
//            path += "/";
//            if (pic_file != null && !pic_file.isEmpty()) {
////                if (configServerApplication.getUploadImageFileExts().contains(pic_file.getName())) {
////                    throw new BusinessException("图片格式不正确！");
////                }
////                if (Long.parseLong(configServerApplication.getUploadImageFileMaxSize()) < pic_file.getSize()) {
////                    throw new BusinessException("图片超出大小限制！");
////                }
//                FileUploadUtils.upload(Global.getUploadPath() + path, fileName, pic_file.getBytes());
//                return StringUtils.format("/upload/{}{}?r={}", path, fileName, RandomUtil.generateRandomNumber(3));
//            }
//            if (isMast) {
//                throw new BusinessException("请上传正确的图片！");
//            }
//            return null;
//        } catch (Exception e) {
//            throw new BusinessException(e.getMessage());
//        }
//    }
}
