package com.ruoyi.wxapi.controller;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.smdd.yhgl.domain.KbsmUser;
import com.ruoyi.smdd.yhgl.service.IKbsmUserService;
import com.ruoyi.wxapi.controller.util.JWTUtil;
import com.ruoyi.wxapi.controller.util.TencentMapUtil;
import com.ruoyi.wxapi.model.domain.UserAddress;
import com.ruoyi.wxapi.model.dto.Result;
import com.ruoyi.wxapi.model.dto.UserAddressDTO;
import com.ruoyi.wxapi.model.qo.BaseParamQO;
import com.ruoyi.wxapi.model.qo.UserAddressQO;
import com.ruoyi.wxapi.model.service.IUserAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/wxapi/address")
@RestController
public class WxapiAddressController {

    @Autowired
    IUserAddressService userAddressService;
//    @Autowired
//    IUserService userService;
    @Autowired
    IKbsmUserService kbsmUserService;

    @RequestMapping("/getLocation")
    public Result getLocation(BaseParamQO baseParamQO) {
        JSONObject locationObj = TencentMapUtil.getLocation(baseParamQO.getLocation());
        System.out.println(locationObj.getJSONObject("result"));
        String recommend = locationObj.getJSONObject("result").getJSONObject("formatted_addresses").getString("recommend");
        JSONObject location = locationObj.getJSONObject("result").getJSONObject("address_component");
        JSONObject data = new JSONObject();
        data.put("location", location);
        data.put("recommend", recommend);
        return Result.success(data);

    }

    @RequestMapping("/lists")
    public Result lists(BaseParamQO baseParamQO) {
        KbsmUser user = kbsmUserService.selectKbsmUserById(Long.valueOf(JWTUtil.parseJWT(baseParamQO.getToken())));
        List<UserAddress> userAddresses = userAddressService.selectByUserIdAndWxappId(JWTUtil.parseJWT(baseParamQO.getToken()), baseParamQO.getWxapp_id());
        List<UserAddressDTO> userAddressDTOS = new ArrayList<>();
        for (int i = 0; i < userAddresses.size(); i++) {
            UserAddressDTO userAddressDTO = new UserAddressDTO();
            BeanUtil.copyProperties(userAddresses.get(i), userAddressDTO);
            userAddressDTOS.add(userAddressDTO);
        }
        JSONObject data = new JSONObject();
        data.put("list", userAddressDTOS);
        data.put("default_id", user.getAddressId());

        return Result.success(data);
    }

    @RequestMapping("/detail")
    public Result detail(BaseParamQO baseParamQO, Integer address_id) {
        UserAddress userAddress = userAddressService.getByAddressIdAndUserId(address_id, JWTUtil.parseJWT(baseParamQO.getToken()));
        UserAddressDTO userAddressDTO = new UserAddressDTO();
        BeanUtil.copyProperties(userAddress, userAddressDTO);
        JSONObject data = new JSONObject();
        data.put("detail", userAddressDTO);
        data.put("disabled", false);
        data.put("nav_select", false);
        return Result.success(data);
    }

    @RequestMapping("/edit")
    public Result edit(BaseParamQO baseParamQO, Integer address_id, UserAddressQO userAddressQO) {
        UserAddress userAddress = new UserAddress();
        BeanUtil.copyProperties(userAddressQO, userAddress);
        userAddress.setUserId(JWTUtil.parseJWT(baseParamQO.getToken()));
        userAddress.setId(address_id);
        Integer flag = userAddressService.updateByUserAdressIdAndUserId(userAddress);

        return Result.success();
    }

    @RequestMapping("/delete")
    public Result delete(BaseParamQO baseParamQO, Integer address_id) {
        Integer flag = userAddressService.deleteByAddressIdandUserId(address_id, JWTUtil.parseJWT(baseParamQO.getToken()));
        if (flag > 0) {
            return Result.success();
        } else {
            return Result.error("删除失败");
        }
    }

    @RequestMapping("/add")
    public Result add(BaseParamQO baseParamQO, UserAddressQO userAddressQO) {
        UserAddress userAddress = new UserAddress();
        BeanUtil.copyProperties(userAddressQO, userAddress);
        userAddress.setUserId(JWTUtil.parseJWT(baseParamQO.getToken()));
        userAddress.setWxappId(baseParamQO.getWxapp_id());
        Integer flag = userAddressService.insertUserAddress(userAddress);
        if (flag > 0) {
            return Result.success();
        } else {
            return Result.error("添加失败");
        }
    }

    @RequestMapping("/setDefault")
    public Result setDefault(BaseParamQO baseParamQO, Integer address_id) {
        KbsmUser user = kbsmUserService.selectKbsmUserById(Long.valueOf(JWTUtil.parseJWT(baseParamQO.getToken())));
        user.setAddressId(Long.valueOf(address_id));
        int flag = kbsmUserService.updateKbsmUser(user);
        if (flag > 0) {
            return Result.success();
        } else {
            return Result.error("设置默认地址失败");
        }
    }

}
