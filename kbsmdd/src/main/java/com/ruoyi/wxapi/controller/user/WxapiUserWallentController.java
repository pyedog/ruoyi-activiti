package com.ruoyi.wxapi.controller.user;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.binarywang.wxpay.bean.request.WxPayUnifiedOrderRequest;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import com.ruoyi.smdd.ddjlgl.domain.KbsmRecharge;
import com.ruoyi.smdd.ddjlgl.service.IKbsmRechargeService;
import com.ruoyi.wxapi.config.AppPropConfig;
import com.ruoyi.wxapi.controller.util.JWTUtil;
import com.ruoyi.wxapi.model.domain.RechargePlan;
import com.ruoyi.wxapi.model.domain.User;
import com.ruoyi.wxapi.model.dto.Result;
import com.ruoyi.wxapi.model.dto.WxpayMpOrderResutDTO;
import com.ruoyi.wxapi.model.qo.BaseParamQO;
import com.ruoyi.wxapi.model.qo.RechargeQO;
import com.ruoyi.wxapi.model.service.IRechargePlanService;
import com.ruoyi.wxapi.model.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/wxapi/user/wallent")
@RestController
public class WxapiUserWallentController {
    @Autowired
    IKbsmRechargeService rechargeService;
    @Autowired
    IRechargePlanService rechargePlanService;
    @Autowired
    AppPropConfig appPropConfig;
    @Autowired
    private IUserService userService;
    @Autowired
    private WxPayService wxService;

    @RequestMapping("/plan")
    public Result plan(BaseParamQO baseParamQO) {
        List<RechargePlan> rechargePlans = rechargePlanService.selectByWxAppId(baseParamQO.getWxapp_id());
        JSONArray array = new JSONArray();
        for (int i = 0; i < rechargePlans.size(); i++) {
            RechargePlan rechargePlan = rechargePlans.get(i);
            JSONObject item = new JSONObject();
            item.put("money", rechargePlan.getMoney());
            item.put("recharge_plan_id", rechargePlan.getId());
            item.put("gift_money", rechargePlan.getGiftMoney());
            array.add(item);
        }
        return Result.success(array);
    }

    @RequestMapping("/recharge/pay")
    public Result rechargePay(BaseParamQO baseParamQO, RechargeQO rechargeQO) throws WxPayException {
        KbsmRecharge recharge = rechargeService.addRecharge(baseParamQO, rechargeQO);
        Integer userId = JWTUtil.parseJWT(baseParamQO.getToken());
        User user = userService.selectByUserId(userId);

        WxPayUnifiedOrderRequest wxPayUnifiedOrderRequest = new WxPayUnifiedOrderRequest();
        wxPayUnifiedOrderRequest.setBody("订单号:" + recharge.getOrderNo());
        wxPayUnifiedOrderRequest.setOutTradeNo(recharge.getOrderNo().toString());
        wxPayUnifiedOrderRequest.setNotifyUrl(appPropConfig.getDomain() + "/wxapi/pay/rechargeNotify");
        wxPayUnifiedOrderRequest.setTotalFee( (int)(recharge.getMoney() * 100));
        wxPayUnifiedOrderRequest.setTradeType("JSAPI");
        wxPayUnifiedOrderRequest.setSpbillCreateIp("127.0.0.1");
        wxPayUnifiedOrderRequest.setOpenid(user.getOpenId());
//        WxPayMpOrderResult wxPayMpOrderResult =  wxService.createOrder(wxPayUnifiedOrderRequest);
        WxpayMpOrderResutDTO wxpayMpOrderResutDTO = new WxpayMpOrderResutDTO();
//        BeanUtil.copyProperties(wxPayMpOrderResult, wxpayMpOrderResutDTO);

        return Result.success(wxpayMpOrderResutDTO);
    }

}
