package com.ruoyi.wxapi.model.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.wxapi.model.domain.Shop;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author hnkb
 * @since 2020-07-04
 */
public interface ShopMapper extends BaseMapper<Shop> {

    Shop getByWxappIdAndShopId(@Param("wxapp_id") Integer wxappId, @Param("shop_id") Integer shopId);

    List<Shop> selectByWxappId(Integer wxappId);
}
