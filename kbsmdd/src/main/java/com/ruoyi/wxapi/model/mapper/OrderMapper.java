package com.ruoyi.wxapi.model.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.wxapi.model.domain.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author hnkb
 * @since 2020-07-14
 */
public interface OrderMapper extends BaseMapper<Order> {

    Integer insertSelective(Order order);

    Integer updatePayStatusByOrderId(@Param("orderNo") Long orderNo, @Param("payStatus") Integer payStatus);

    List<Order> selectLargeByTableId(@Param("tableId") Integer tableId);

    List<Order> selectByTableId(@Param("tableId") Integer tableId);

    List<Order> selectByTableIdAndUserId(@Param("tableId") Integer tableId, @Param("userId") Integer userId);

    List<Order> selectLargeByTableIdAndUserId(@Param("tableId") Integer tableId, @Param("userId") Integer userId);

    Order selectByOrderId(@Param("orderId") Integer orderId);

    Order selectByOrderIdAndUserId(@Param("orderId") Integer orderId, @Param("userId") Integer userId);
}
