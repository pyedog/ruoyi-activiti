package com.ruoyi.wxapi.model.service.impl;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.ruoyi.wxapi.controller.util.JWTUtil;
import com.ruoyi.wxapi.model.domain.Recharge;
import com.ruoyi.wxapi.model.domain.RechargePlan;
import com.ruoyi.wxapi.model.mapper.RechargeMapper;
import com.ruoyi.wxapi.model.mapper.RechargePlanMapper;
import com.ruoyi.wxapi.model.qo.BaseParamQO;
import com.ruoyi.wxapi.model.qo.RechargeQO;
import com.ruoyi.wxapi.model.service.IRechargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hnkb
 * @since 2020-07-21
 */
@Service
public class RechargeServiceImpl extends ServiceImpl<RechargeMapper, Recharge> implements IRechargeService {
    @Autowired
    RechargePlanMapper rechargePlanMapper;

    @Override
    public Recharge addRecharge(BaseParamQO baseParamQO, RechargeQO rechargeQO) {
        Snowflake snowflake = IdUtil.getSnowflake(1, 1);
        long orderNo = snowflake.nextId();
        Recharge recharge = null;
        if (rechargeQO.getRecharge_plan_id() > 0) {
            // 套餐充值
            RechargePlan rechargePlan = rechargePlanMapper.getByRechargePlanId(rechargeQO.getRecharge_plan_id());
            recharge = new Recharge();
            recharge.setOrderNo(orderNo);
            recharge.setMoney(rechargePlan.getMoney());
            recharge.setGiftMoney(rechargePlan.getGiftMoney());
            recharge.setUserId(JWTUtil.parseJWT(baseParamQO.getToken()));
            recharge.setShopId(baseParamQO.getShop_id());
            recharge.setWxappId(baseParamQO.getWxapp_id());
            recharge.setRechargePlanId(rechargePlan.getId());

        } else {
            // 直接充值
            recharge = new Recharge();
            recharge.setOrderNo(orderNo);
            recharge.setMoney(rechargeQO.getMoney());
            recharge.setUserId(JWTUtil.parseJWT(baseParamQO.getToken()));
            recharge.setShopId(baseParamQO.getShop_id());
            recharge.setWxappId(baseParamQO.getWxapp_id());

        }
        Integer flag = this.getBaseMapper().insert(recharge);
        if (flag > 0) {
            return recharge;
        }
        return null;
    }

    @Override
    public Recharge getByOrderNo(long orderNo) {
        return this.getBaseMapper().getByOrderNo(orderNo);
    }
}
