package com.ruoyi.wxapi.model.service.impl;

import com.ruoyi.wxapi.model.domain.KbsmUploadGroup;
import com.ruoyi.wxapi.model.mapper.KbsmUploadGroupMapper;
import com.ruoyi.wxapi.model.service.IKbsmUploadGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hnkb
 * @since 2020-08-05
 */
@Service
public class KbsmUploadGroupServiceImpl extends ServiceImpl<KbsmUploadGroupMapper, KbsmUploadGroup> implements IKbsmUploadGroupService {

}
