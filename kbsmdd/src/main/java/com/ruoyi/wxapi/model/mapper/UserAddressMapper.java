package com.ruoyi.wxapi.model.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.wxapi.model.domain.UserAddress;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author hnkb
 * @since 2020-07-23
 */
public interface UserAddressMapper extends BaseMapper<UserAddress> {

    List<UserAddress> selectByUserIdAndWxappId(@Param("userId") Integer userId, @Param("wxappId") Integer wxappId);

    UserAddress getByAddressId(Integer addressId);

    UserAddress getByAddressIdAndUserId(@Param("addressId") Integer addressId, @Param("userId") Integer userId);

    Integer updateByUserAdressIdAndUserId(UserAddress userAddress);

    Integer deleteByAddressIdandUserId(@Param("addressId") Integer addressId, @Param("userId") Integer userId);

    Integer insertUserAddress(UserAddress userAddress);
}
