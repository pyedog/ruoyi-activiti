package com.ruoyi.wxapi.model.mapper;

import com.ruoyi.wxapi.model.domain.KbsmUploadGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hnkb
 * @since 2020-08-05
 */
public interface KbsmUploadGroupMapper extends BaseMapper<KbsmUploadGroup> {

}
