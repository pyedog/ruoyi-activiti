package com.ruoyi.wxapi.model.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.wxapi.model.domain.OrderAddress;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hnkb
 * @since 2020-07-14
 */
public interface IOrderAddressService extends IService<OrderAddress> {

    OrderAddress selectByOrderNoAndUserId(Long orderNo, Integer userId);
}
