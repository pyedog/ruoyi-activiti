package com.ruoyi.wxapi.model.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.wxapi.model.domain.WxappHelp;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hnkb
 * @since 2020-07-23
 */
public interface IWxappHelpService extends IService<WxappHelp> {

    List<WxappHelp> selectByWxappId(@Param("wxappId") Integer wxappId);
}
