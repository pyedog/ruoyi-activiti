package com.ruoyi.wxapi.model.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.wxapi.model.domain.GoodsSpecGroup;
import com.ruoyi.wxapi.model.mapper.GoodsSpecGroupMapper;
import com.ruoyi.wxapi.model.service.IGoodsSpecGroupService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hnkb
 * @since 2020-07-10
 */
@Service
public class GoodsSpecGroupServiceImpl extends ServiceImpl<GoodsSpecGroupMapper, GoodsSpecGroup> implements IGoodsSpecGroupService {

}
