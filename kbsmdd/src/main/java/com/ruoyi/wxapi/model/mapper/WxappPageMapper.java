package com.ruoyi.wxapi.model.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.wxapi.model.domain.WxappPage;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author hnkb
 * @since 2020-07-01
 */
public interface WxappPageMapper extends BaseMapper<WxappPage> {

    WxappPage selectByWxAppId(Integer wxappId);
}
