package com.ruoyi.wxapi.model.service;

import com.ruoyi.wxapi.model.domain.KbsmUploadGroup;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hnkb
 * @since 2020-08-05
 */
public interface IKbsmUploadGroupService extends IService<KbsmUploadGroup> {

}
