package com.ruoyi.wxapi.model.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.wxapi.model.domain.Recharge;
import com.ruoyi.wxapi.model.qo.BaseParamQO;
import com.ruoyi.wxapi.model.qo.RechargeQO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hnkb
 * @since 2020-07-21
 */
public interface IRechargeService extends IService<Recharge> {

    Recharge addRecharge(BaseParamQO baseParamQO, RechargeQO rechargeQO);

    Recharge getByOrderNo(long orderNo);
}
