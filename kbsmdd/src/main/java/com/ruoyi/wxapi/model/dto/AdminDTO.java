package com.ruoyi.wxapi.model.dto;

import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.wxapi.model.domain.Admin;
import lombok.Data;

import java.util.List;

@Data
public class AdminDTO {
    private Admin admin;
    private List<SysRole> roles;
}
