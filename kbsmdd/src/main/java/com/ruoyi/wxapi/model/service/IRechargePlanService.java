package com.ruoyi.wxapi.model.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.wxapi.model.domain.RechargePlan;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hnkb
 * @since 2020-07-21
 */
public interface IRechargePlanService extends IService<RechargePlan> {

    RechargePlan getByWxAppId(@Param("wxappId") Integer wxappId);

    List<RechargePlan> selectByWxAppId(@Param("wxappId") Integer wxappId);
}
