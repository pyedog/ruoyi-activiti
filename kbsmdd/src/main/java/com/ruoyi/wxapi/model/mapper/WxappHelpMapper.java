package com.ruoyi.wxapi.model.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.wxapi.model.domain.WxappHelp;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hnkb
 * @since 2020-07-23
 */
public interface WxappHelpMapper extends BaseMapper<WxappHelp> {

    List<WxappHelp> selectByWxappId(Integer wxappId);
}
