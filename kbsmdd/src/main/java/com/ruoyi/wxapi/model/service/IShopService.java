package com.ruoyi.wxapi.model.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.wxapi.model.domain.Shop;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hnkb
 * @since 2020-07-04
 */
public interface IShopService extends IService<Shop> {

    Shop getByWxappIdAndShopId(@Param("wxapp_id") Integer wxapp_id, @Param("shop_id") Integer shop_id);

    List<Shop> selectByWxappId(@Param("wxapp_id") Integer wxapp_id);
}
