package com.ruoyi.wxapi.model.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.wxapi.model.domain.UserAddress;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hnkb
 * @since 2020-07-23
 */
public interface IUserAddressService extends IService<UserAddress> {

    List<UserAddress> selectByUserIdAndWxappId(@Param("userId") Integer userId, @Param("wxappId") Integer wxappId);

    UserAddress getByAddressId(Integer addressId);

    UserAddress getByAddressIdAndUserId(Integer address_id, Integer parseJWT);

    Integer updateByUserAdressIdAndUserId(UserAddress userAddress);

    Integer deleteByAddressIdandUserId(Integer address_id, Integer parseJWT);

    Integer insertUserAddress(UserAddress userAddress);
}
