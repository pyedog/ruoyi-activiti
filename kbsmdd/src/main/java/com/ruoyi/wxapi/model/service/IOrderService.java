package com.ruoyi.wxapi.model.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.wxapi.model.domain.Cart;
import com.ruoyi.wxapi.model.domain.Order;
import com.ruoyi.wxapi.model.qo.BaseParamQO;
import com.ruoyi.wxapi.model.qo.OrderQO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hnkb
 * @since 2020-07-14
 */
public interface IOrderService extends IService<Order> {

    Order addOrder(List<Map.Entry<String, Cart>> list, BaseParamQO baseParamQO, OrderQO orderQO);

    Integer updatePayStatusByOrderId(@Param("orderNo") Long orderNo, @Param("payStatus") Integer payStatus);

    List<Order> selectLargeByTableId(Integer tableId);

    List<Order> selectByTableId(Integer tableId);

    List<Order> selectByTableIdAndUserId(@Param("tableId") Integer tableId, @Param("userId") Integer userId);

    List<Order> selectLargeByTableIdAndUserId(@Param("tableId") Integer tableId, @Param("userId") Integer userId);

    Order selectByOrderId(Integer order_id);

    Order selectByOrderIdAndUserId(@Param("orderId") Integer orderId, @Param("userId") Integer userId);
}
