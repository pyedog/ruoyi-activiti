package com.ruoyi.wxapi.model.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.wxapi.model.domain.UserGrade;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hnkb
 * @since 2020-07-26
 */
public interface IUserGradeService extends IService<UserGrade> {

    UserGrade getByUserGradeId(Integer userGradeId);

    List<UserGrade> selectByWxAppId(@Param("wxapp_id") Integer wxapp_id);
}
