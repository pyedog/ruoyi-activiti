package com.ruoyi.wxapi.model.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.wxapi.model.domain.WxappPage;
import com.ruoyi.wxapi.model.mapper.WxappPageMapper;
import com.ruoyi.wxapi.model.service.IWxappPageService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author hnkb
 * @since 2020-07-01
 */
@Service
public class WxappPageServiceImpl extends ServiceImpl<WxappPageMapper, WxappPage> implements IWxappPageService {

    @Override
    public WxappPage selectByWxAppId(Integer wxappId) {
        return this.getBaseMapper().selectByWxAppId(wxappId);
    }
}
