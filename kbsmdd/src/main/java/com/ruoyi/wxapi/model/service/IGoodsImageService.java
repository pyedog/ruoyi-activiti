package com.ruoyi.wxapi.model.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.wxapi.model.domain.GoodsImage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hnkb
 * @since 2020-07-10
 */
public interface IGoodsImageService extends IService<GoodsImage> {

    GoodsImage getByGoodsId(Integer id);

    List<GoodsImage> selectByGoodsId(@Param("goods_id") Integer goods_id);
}
