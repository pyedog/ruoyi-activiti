package com.ruoyi.wxapi.model.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.wxapi.model.domain.User;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hnkb
 * @since 2020-07-01
 */
public interface IUserService extends IService<User> {

    User selectByOpenId(String openid);

    User selectByUserId(Integer userId);

    Integer insertUser(User user);
}
