package com.ruoyi.wxapi.model.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.wxapi.model.domain.WxappPage;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hnkb
 * @since 2020-07-01
 */
public interface IWxappPageService extends IService<WxappPage> {

    WxappPage selectByWxAppId(@Param("wxapp_id") Integer wxapp_id);
}
